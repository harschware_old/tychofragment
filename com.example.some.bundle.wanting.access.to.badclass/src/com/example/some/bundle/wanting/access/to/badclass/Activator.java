package com.example.some.bundle.wanting.access.to.badclass;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import com.example.untouchable.plugin.badclass.FactoryForClassWithPackagePrivateConstructor;


public class Activator extends AbstractUIPlugin {

	public void start(BundleContext context) throws Exception {
		super.start(context);

		// create a new instance of ClassWithPackagePrivateConstructor, which
		// will print something to stdout
		FactoryForClassWithPackagePrivateConstructor.getInstance();
	} // end method

} // end class
